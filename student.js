import { Person } from "./person.js";

class Student extends Person {
    constructor(personName, personAge, gender, standard, collegeName, grade) {
        super(personName, personAge, gender);
        this.standard = standard;
        this.collegeName = collegeName;
        this.grade = grade

    }
    getStudentInfo() {
        return `Student name: ${this.personName}
Age:  ${this.personAge}
Gender: ${this.gender}
Standard: ${this.standard}
College name: ${this.collegeName}
`
    }
    getGrade() {
        return `Grade: ${this.grade}
`
    }
}
export default Student
