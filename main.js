import { Person } from "./person.js";
import Student from "./student.js";
let person1 = new Person("Bao", 18, "male");
console.log(person1.getPersonInfo());
console.log(person1 instanceof Person)

let person2 = new Person("Gia", 38, "female");
console.log(person2.getPersonInfo());
console.log(person2 instanceof Person);

let person3 = new Person("Nam", 28, "male");
console.log(person3.getPersonInfo());
console.log(person3 instanceof Person);


let student1 = new Student("Hoang", 19, "male", "A", "Devcamp", 9);
console.log(student1.getPersonInfo());
console.log(student1.getStudentInfo());
console.log(student1.getGrade())
console.log(student1 instanceof Student)

let student2 = new Student("Minh", 29, "female", "B", "Devcamp", 8);
console.log(student2.getPersonInfo())
console.log(student2.getStudentInfo())
console.log(student2.getGrade())
console.log(student2 instanceof Student)


let student3 = new Student("Thao", 29, "female", "C", "Devcamp", 8.5);
console.log(student3.getPersonInfo())
console.log(student3.getStudentInfo())
console.log(student3.getGrade())
console.log(student3 instanceof Student)